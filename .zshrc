#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' matcher-list 'r:|[._-]=** r:|=**'
zstyle :compinstall filename '/home/decko/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
export PATH="/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:/home/decko/.local/bin:/home/decko/bin:~/dev/flutter/bin"


# You may need to manually set your language environment
# export LANG=en_US.UTF-8
export LANG=pt_BR.UTF-8


function copycat() {
	if [ -n "$1" ]
	then
		cat $1 | xsel -i -b
	fi
}


# Load our user defined aliases.
[ -f ~/.aliases ] && source ~/.aliases

# alias laravel='docker run --rm -v $PWD:/home/laravel/workdir -ti laradev laravel'
# alias artisan='docker run --rm -v $PWD:/home/laravel/workdir -p 8000:8000 -ti laradev php artisan'
# alias composer='docker run --rm -v $PWD:/home/laravel/workdir -ti laradev composer'

export NVM_DIR="/home/decko/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export WORKON_HOME=~/.virtualenvs
export PROJECT_HOME=~/dev

export PATH="/home/decko/.pyenv/bin:$PATH"
eval "$(pyenv init -)"

# export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
pyenv virtualenvwrapper_lazy

# Add cargo build by rustup.
export PATH="$HOME/.cargo/bin:$PATH"

# Add YARN prefix to the PATH.
export PATH="$HOME/.yarn/bin:$PATH"

# Add Meteor prefix to the PATH.
export PATH="$HOME/.meteor:$PATH"

# Add PHP's Composer vendor prefix to the PATH.
export PATH="$HOME/.config/composer/vendor/bin:$PATH"

# Make firefox scroll one-to-one trackpad scrolling
export MOZ_USE_XINPUT2=1
export GDK_BACKEND=wayland

# Enable PasswordStore(pass) extensions
export PASSWORD_STORE_ENABLE_EXTENSIONS=true

export GSM_SKIP_SSH_AGENT_WORKAROUND=1

# Use nvim as my default terminal editor
export EDITOR=nvim

# Set the path to minishift
export PATH="/home/decko/Downloads/minishift-1.26.1-linux-amd64:$PATH"
export PATH="/home/decko/.minishift/cache/oc/v3.11.0/linux:$PATH"

# Set path to snap binaries
export PATH="/var/lib/snapd/snap/bin:$PATH"

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
#[[ -f /home/decko/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.zsh ]] && . /home/decko/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.zsh

. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash
