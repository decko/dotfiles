set runtimepath+=~/.vim/plugged/vim-plug
"set encoding=utf-8

set nocompatible
" filetype plugin indent on
" syntax enable
set exrc

let vimplug=expand('~/.vim/plugged/vim-plug/plug.vim')
if !filereadable(vimplug)
	echo "Instaling vim-plug..."
	echo ""
	silent !mkdir -p ~/.vim/
	silent !git clone https://github.com/junegunn/vim-plug.git ~/.vim/plugged/vim-plug
	let g:first_time_run=1
endif

" Scotty, beam me up!
source ~/.vim/plugged/vim-plug/plug.vim

function! BuildComposer(info)
  if a:info.status != 'unchanged' || a:info.force
    if has('nvim')
      !cargo build --release
    else
      !cargo build --release --no-default-features --features json-rpc
    endif
  endif
endfunction

call plug#begin('~/.vim/plugged')

" Suppa generic stuff
Plug 'junegunn/vim-plug'
Plug 'editorconfig/editorconfig-vim'
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-commentary'
" Plug 'w0rp/ale'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'dense-analysis/ale'
Plug 'yuttie/comfortable-motion.vim'
Plug 'gregsexton/gitv', {'on': ['Gitv']}
Plug 'vim-scripts/bufkill.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'

" Plug 'nathanaelkane/vim-indent-guides'
Plug 'Yggdroot/indentLine'
let g:indentLine_faster = 1
" let g:indentLine_setConceal = 0

" junegunn's substitute to CtrlP as a fuzzy file finder
" Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Plug 'junegunn/fzf.vim', {'on': ['Files']}
Plug 'lotabout/skim.vim', {'on': ['Files']}

" HTML/CSS and JS stuff
Plug 'othree/html5.vim', { 'for': 'html' }
Plug 'othree/yajs.vim', { 'for': 'javascript' }
Plug 'jelera/vim-javascript-syntax', { 'for': 'javascript' }
Plug 'othree/javascript-libraries-syntax.vim', { 'for': 'javascript' }
Plug 'hail2u/vim-css3-syntax', { 'for': 'css' }
Plug 'alvan/vim-closetag', { 'for': 'html' }
Plug 'HerringtonDarkholme/yats.vim', { 'for': 'typescript' }

" LanguageServers
Plug 'JakeBecker/elixir-ls', {'for': 'elixir', 'do': 'mix deps.get && mix compile && mix elixir_ls.release -o .'}

" Elixir goodness
Plug 'elixir-editors/vim-elixir', { 'for': 'elixir'}

" Python dev
Plug 'psf/black', {'for': 'python'}
Plug 'hdima/python-syntax', { 'for': 'python' }
Plug 'tmhedberg/SimpylFold', { 'for': 'python' }
Plug 'vim-scripts/indentpython.vim', { 'for': 'python' }

" LaTeX support
Plug 'lervag/vimtex', { 'for': 'latex' }

" Testing
Plug 'janko-m/vim-test', { 'on': ['TestNearest', 'TestLast', 'TestSuite'] }

" Code Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Cool git stuff
Plug 'tpope/vim-fugitive'

" Color Schemes
" Plug 'altercation/vim-colors-solarized'
Plug 'chriskempson/base16-vim'
" Plug 'aereal/vim-colors-japanesque'
" Plug 'morhetz/gruvbox'
" Plug 'trevordmiller/nova-vim'
" Plug 'jnurmine/Zenburn'
" Plug 'liuchengxu/space-vim-dark'
" Plug 'rakr/vim-one'
" Plug 'NLKNguyen/papercolor-theme'

" NERDTree and good stuff alike
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'tiagofumo/vim-nerdtree-syntax-highlight', {'on': 'NERDTreeToggle'}
Plug 'ryanoasis/vim-devicons', {'on': 'NERDTreeToggle'}
Plug 'majutsushi/tagbar', {'on': 'TagbarToggle'}
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }

" Tmux support
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'christoomey/vim-tmux-navigator'

" Markdown support
Plug 'euclio/vim-markdown-composer', { 'for': 'markdown', 'do': function('BuildComposer') }

" Dockerfile support
Plug 'ekalinin/Dockerfile.vim', { 'for': 'Dockerfile' }

" YAML support
Plug 'stephpy/vim-yaml', { 'for': 'yaml' }

" RST support
Plug 'gu-fan/InstantRst', { 'for': 'rst' }

call plug#end()

if exists('g:first_time_run')
	PlugUpdate
endif


" Make all yank operations use the system clipboard
set clipboard+=unnamedplus

" Make search work as expected
set nohlsearch incsearch ignorecase smartcase

" And do some visual configuration
set number " Show line numbers
set ruler " Always show the ruler
set showcmd " Show command in status line
set showmatch " Show matching brackets
set showmode " Show what mode you're currently in
set cursorline " Highlight cursor line
set scrolloff=5 " Let 5 lines before scrolling
set wrap linebreak " Break the lines that exceed textwidth
set laststatus=2 " Always shows the status line
set updatetime=300 " Smaller updatetime for CursorHold & CursorHoldI
set shortmess+=c " don't give ins-completion-menu messages.

" Enable folding
set foldenable
set foldmethod=indent
set foldlevel=99
let g:SimpylFold_docstring_preview=1
let g:SimpylFold_fold_docstring = 0
let g:SimpylFold_fold_import = 0
" autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
" autocmd BufWinLeave *.py setlocal foldexpr< foldmethod<
autocmd FileType python set autoindent

" " And enable some exceptions
" set tabstop=4
" set shiftwidth=4
" set expandtab

autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2 shiftround expandtab
" autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2 shiftround expandtab

" No extra spaces when joining lines
set nojoinspaces
" Interpret numbers with leading zeroes as decimal, not octal
set nrformats=
" Auto-format comments
set formatoptions+=roq

autocmd BufNewFile,BufReadPost *.ino,*.pde set filetype=arduino

" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes+=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc,.pyc
set wildignore+=*.bak,~*,*.swp,*.o,*.info,*.aux,*.log,*.dvi,*.bbl,*.blg,*.brf,*.cb,*.ind,*.idx,*.ilg,*.inx,*.out,*.toc,*.pyc"

"Helping Python Indentation
highlight BadWhitespace ctermbg=red guibg=red

let python_highlight_all = 1

au BufNewFile,BufRead *.py
 			\ setlocal tabstop=4
 			\ softtabstop=4
 			\ shiftwidth=4
			\ textwidth=110
 			\ expandtab
 			\ autoindent
 			\ fileformat=unix

au BufNewFile,BufRead *.py match BadWhitespace /\s\+$/

" Fix backspace indent
set backspace=indent,eol,start

"" Map leader to ,
let mapleader=','

"" Enable hidden buffers
set hidden

"" Encoding
set bomb
set binary
set ttyfast

"" Directories for swp files
set nobackup
set noswapfile

set fileformats=unix,dos,mac
set showcmd
set shell=/bin/sh

" Reload files automatically
set autoread

"" Configuring vim-instant-markdown"
let g:instant_markdown_autostart=0

"" Configuring darksolarized colorscheme
" set term=screen-256color
" let no_buffers_menu=1
set termguicolors
" set Vim-specific sequences for RGB colors
" let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
" set t_Co=256
let g:solarized_termtrans=0
" colorscheme one
set background=dark
" call togglebg#map("<F3>")
colorscheme base16-gruvbox-dark-hard
" colorscheme base16-paraiso
" colorscheme japanesque
" colorscheme gruvbox
" colorscheme nova
" colorscheme space-vim-dark
" colorscheme PaperColor

set relativenumber

set mousemodel=popup

"" Use modeline overrides
set modeline
set modelines=10

set title
set titleold="Terminal"
set titlestring=%F


" vim-airline
" let g:airline_theme = 'one'
" let g:airline_theme = 'papercolor'
" let g:airline_theme = 'powerlineish'
" let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#neomake#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1

set ttimeoutlen=50

"" Abbreviations

cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

" Configuring UltiSnips
let g:UltiSnipsListSnippets = "<c-k>"
let g:UltiSnipsUsePythonVersion = 3

" autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
" autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
" autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

"""*****************************************************************************
"" Mappings
"""*****************************************************************************
""" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>
set splitright
set splitbelow

" ctrlp mapping to FZF fuzzy file finder
" http://www.devinmorgenstern.com/2016/02/16/ditching-ctrl-p/
map <C-p> :Files<cr>
nmap <C-p> :Files<cr>

" and a tip from @cassiobotaro @ https://gist.github.com/cassiobotaro/619554ebfef36671c823a2ea5b0b592a
" function! RipgrepFzf(query, fullscreen)
"   let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
"   let initial_command = printf(command_fmt, shellescape(a:query))
"   let reload_command = printf(command_fmt, '{q}')
"   let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
"   call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
" endfunction

" command! -nargs=* -bang Rg call RipgrepFzf(<q-args>, <bang>0)

" let $FZF_DEFAULT_COMMAND = 'ag -g ""'
let $FZF_DEFAULT_COMMAND = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
nnoremap <silent> <leader>f :Ag<CR>
nnoremap <silent> <leader>b :Buffers<CR>
nnoremap <silent> <leader>e :FZF -m<CR>

"" Buffer nav
noremap <S-Tab>   :bp<CR>
noremap <leader>z :bp<CR>
noremap <leader>q :bp<CR>

noremap <Tab>     :bn<CR>
noremap <leader>x :bn<CR>
noremap <leader>w :bn<CR>

"" Close buffer
noremap <leader>c :BD<CR>"

" Tagbar
nmap <silent> <F4> :TagbarToggle<CR>
let g:tagbar_autoclose = 1

" Split Navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Enable folding with the spacebar
nnoremap <space> za

let g:python_host_prog = '~/.pyenv/versions/2.7.16/envs/neovim2/bin/python'
let g:python3_host_prog = expand('~/.pyenv/versions/3.6.8/envs/neovim3/bin/python')

set colorcolumn=+1

" String to put at the start of lines that have been wrapped "
let &showbreak='↪ '

" set the movement keys to a harder, but better way
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk

" set ; to behave like :
nnoremap ; :

" get rid of all trailling spaces using <leader>W
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" Remap number increment
nmap <UP> <C-A>
nmap <DOWN> <C-X>

"" Mappings for vim-test
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

"" Test strategies
let test#strategy = {
	\ 'nearest': 'neovim',
	\ 'file': 'neovim',
	\ 'suite': 'neovim',
\}
let test#python#runner = 'pytest'
let test#python#pytest#options = '-W ignore -s'
let test#python#pytest#file_pattern = 'test'

" Configure ALE error msgs.
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {
	\ 'javascript': ['eslint'],
	\ 'python': ['pyls', 'flake8'],
	\ 'rust': ['rls'],
	\ 'elixir': ['elixir-ls'],
	\ 'c': ['clangd']
	\}

" Configure flake8 on ALE
let g:ale_python_executable = 'python3'
let g:ale_python_flake8_options = '--exclude=docs/*,*migrations* --ignore=E203,E501,W503'
let g:ale_python_flake8_auto_pipenv = 1
let g:ale_python_pyls_auto_pipenv = 1

let g:ale_elixir_elixir_ls_release = expand('~/.vim/plugged/elixir-ls')

let g:ale_set_balloons = 1
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option('ignore_sources', {'_': ['buffer']})
autocmd CompleteDone * silent! pclose!
set completeopt=longest,menuone,preview

nnoremap <silent> K :ALEHover<CR>
nnoremap <silent> gd :ALEGoToDefinition<CR>
nnoremap <silent> gt :ALEGoToTypeDefinition<CR>
nnoremap <silent> gr :ALEFindReferences<CR>
nnoremap <silent> gx :ALESymbolSearch<CR>

" NERDTree config
" map <F8> :Vexplore<CR>
map <F8> :NERDTreeToggle<CR>
let g:NERDTreeLimitedSyntax = 1
let g:NERDTreeUpdateOnCursorHold = 0

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Toggle tagbar window
nmap <F7> :TagbarToggle<CR>

let $FZF_DEFAULT_COMMAND = 'ag -g ""'
nnoremap <silent> <leader>f :Ag<CR>
nnoremap <silent> <leader>b :Buffers<CR>
nnoremap <silent> <leader>e :FZF -m<CR>

let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

" Disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

nmap gs :Gstatus<CR>
nmap gb :Gblame<CR>
nmap gp :Gpush<CR>
nmap gk :Gitv<CR>
" nmap gl :call OpenGitLog()<CR>
" nmap gq :call CloseGitLog()<CR>
nmap gl :Glog --topo-order --stat --pretty=format:"${_git_log_medium_format}"<CR>
nmap gS :Git show<CR>

function! OpenGitLog()
    " execute(":tabnew % | Glog -- --topo-order --stat --pretty=format:'${_git_log_medium_format}'| copen")
    execute(":tabnew % | :Glog -- | copen")
endfunction
function! CloseGitLog()
   execute(":cclose | q! | tabprevious")
endfunction
