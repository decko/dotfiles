#!/usr/bin/env bash

# If not running interactively, don't do anything
case $- in
  *i*) ;;
    *) return;;
esac

# Path to the bash it configuration
export BASH_IT="$HOME/.bash_it"

# Lock and Load a custom theme file.
# Leave empty to disable theming.
# location /.bash_it/themes/
export BASH_IT_THEME='bobby'

# (Advanced): Change this to the name of your remote repo if you
# cloned bash-it with a remote other than origin such as `bash-it`.
# export BASH_IT_REMOTE='bash-it'

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='git@git.domain.com'

# Don't check mail when opening terminal.
unset MAILCHECK

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Set this to the command you use for todo.txt-cli
export TODO="t"

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true

# Set Xterm/screen/Tmux title with only a short hostname.
# Uncomment this (or set SHORT_HOSTNAME to something else),
# Will otherwise fall back on $HOSTNAME.
#export SHORT_HOSTNAME=$(hostname -s)

# Set Xterm/screen/Tmux title with only a short username.
# Uncomment this (or set SHORT_USER to something else),
# Will otherwise fall back on $USER.
#export SHORT_USER=${USER:0:8}

# Set Xterm/screen/Tmux title with shortened command and directory.
# Uncomment this to set.
#export SHORT_TERM_LINE=true

# Set vcprompt executable path for scm advance info in prompt (demula theme)
# https://github.com/djl/vcprompt
#export VCPROMPT_EXECUTABLE=~/.vcprompt/bin/vcprompt

# (Advanced): Uncomment this to make Bash-it reload itself automatically
# after enabling or disabling aliases, plugins, and completions.
# export BASH_IT_AUTOMATIC_RELOAD_AFTER_CONFIG_CHANGE=1

# Uncomment this to make Bash-it create alias reload.
# export BASH_IT_RELOAD_LEGACY=1

# Load Bash It
source "$BASH_IT"/bash_it.sh

# Setting path
export PATH="/usr/lib64/qt-3.3/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:$HOME/.local/bin:$HOME/bin:~/dev/flutter/bin"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8
export LANG=pt_BR.UTF-8

# Copy the content of a file to clipboard
function copycat() {
	if [ -n "$1" ]
	then
		cat $1 | xsel -i -b
	fi
}

# Load our user defined aliases.
[ -f ~/.aliases ] && source ~/.aliases

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

[ -d /usr/share/skim ] && source /usr/share/skim/shell/key-bindings.bash
# [ -f ~/.fzf.bash ] && source ~/.fzf.bash

export WORKON_HOME=~/.virtualenvs
export PROJECT_HOME=~/dev

export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"

# export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
pyenv virtualenvwrapper_lazy

# Add cargo build by rustup.
export PATH="$HOME/.cargo/bin:$PATH"

# Add YARN prefix to the PATH.
export PATH="$HOME/.yarn/bin:$PATH"

# Add Meteor prefix to the PATH.
export PATH="$HOME/.meteor:$PATH"

# Add PHP's Composer vendor prefix to the PATH.
export PATH="$HOME/.config/composer/vendor/bin:$PATH"

# Make firefox scroll one-to-one trackpad scrolling
export MOZ_USE_XINPUT2=1
# export GDK_BACKEND=wayland
export MOZ_ENABLE_WAYLAND=1

# Enable PasswordStore(pass) extensions
export PASSWORD_STORE_ENABLE_EXTENSIONS=true

export GSM_SKIP_SSH_AGENT_WORKAROUND=1

# Use nvim as my default terminal editor
export EDITOR=nvim

# Set the path to minishift
export PATH="$HOME/Downloads/minishift-1.26.1-linux-amd64:$PATH"
export PATH="$HOME/.minishift/cache/oc/v3.11.0/linux:$PATH"

# Set path to snap binaries
export PATH="/var/lib/snapd/snap/bin:$PATH"

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
#[[ -f $HOME/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.zsh ]] && . $HOME/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.zsh

. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash

export HISTCONTROL=erasedups:ignorespace
export HISTIGNORE="pwd:ls:ps:cd:history"
export HISTSIZE=10000 HISTFILESIZE=10000
PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

bind '"\eh": "\C-a\eb\ed\C-y\e#man \C-y\C-m\C-p\C-p\C-a\C-d\C-e"'

export SKIM_DEFAULT_OPTIONS="--ansi --preview-window 'right:60%' --preview 'bat --color=always --style=header,grid --line-range :300 {}'"
